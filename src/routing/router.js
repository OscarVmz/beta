
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import home from  '../views/home';
import defaultOption from '../views/defaultOption';

const RouterConfig = ( props ) => (
    <div className='contecnt'> 
        <Router>
            <Switch>
                <Route exact={true}
                path='/' 
                component={ home }/> // pathc va la ruta donde se podra ver la vista
                <Route component={ defaultOption }/> //componente 404 not found
            </Switch>
        </Router>
    </div>
);

export default RouterConfig;
