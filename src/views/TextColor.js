import React, { Component } from 'react';

class TextColor extends Component {

    constructor(){
        super()
        this.state = {
            color: true
        }
    }
    
    
    render() {

        const { color: color2 } = this.state;
        
        let contenido = '';

        if( color2 ){
            contenido = 'color1';
            console.log('contenido '+contenido);
        }else{
            contenido = 'color2';
            console.log('contenido '+contenido);
        }
    
        return (
            <div className='container'>
                <div className= { contenido }  >
                    <p >Texto de color</p>
                </div>

                <div className='container'>
                    <input type='button' value='Cambiar color' onClick={this.cambiar} />
                </div>    
            </div>
        );
    }


        cambiar = () => {
            const { color: color1 } = this.state;
            console.log('el color1 es: '+ color1);
        this.setState({
            color: !color1
        });
        console.log('el color2 es: '+ this.state.color);
    } 
}


export default TextColor;