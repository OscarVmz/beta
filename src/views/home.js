import React, { Component } from 'react';
import '../styles/main.scss';
import TextColor from './TextColor';

class home extends Component {
  render() {
    return(
      <div className="App">
        <header className="App-header">
          <h1>Principal</h1>
        </header>
        <div>
          <TextColor />
        </div>
      </div>
    );
  }
}

export default home;
